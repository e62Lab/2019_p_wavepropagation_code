#include <medusa/Medusa.hpp>
#include <Eigen/Sparse>
#include <cmath>

using namespace mm;
using namespace Eigen;

const double pi = 3.14159265358979323846;

/**
 * Scattered node interpolant averaging over small neighbourhood.
 */
template <class vec_t, class value_t>
class ScatteredInterpolant {
    KDTree<vec_t> tree;  ///< Tree of all points.
    Range<value_t> values;  ///< Function values at given points.
    int num_closest;  ///< Number of closest points to include in calculations.
  public:
    /**
     * Creates a relax function that has approximately the same values of `dr` as given nodes.
     * @param pos Positions of given nodes.
     * @param values Function values at given positions.
     * @param num_closest How many neighbours to take into account when calculating function value.
     */
    ScatteredInterpolant(const Range<vec_t>& pos, const Range<value_t>& values, int num_closest)
            : tree(pos), values(values), num_closest(num_closest) {}

    /// Evaluate the distribution function.
    value_t operator()(const vec_t& point) const {
        Range<int> idx = tree.query(point, num_closest).first;
        value_t avg = value_t(0);
        for (int i : idx) {
            avg += values[i];
        }
        return avg / num_closest;
    }

    value_t evaluate(const vec_t& point) const {
        Range<int> idx = tree.query(point, num_closest).first;
        value_t avg = value_t(0);
        for (int i : idx) {
            avg += values[i];
        }
        return avg / num_closest;
    }
};


/**
 * Scattered node interpolant averaging over small neighbourhood.
 * Implements modified Sheppard's method.
 */
template <class vec_t, class value_t>
class ModifiedSheppardsScatteredInterpolant {
    KDTree<vec_t> tree;  ///< Tree of all points.
    Range<value_t> values;  ///< Function values at given points.
    int num_closest;  ///< Number of closest points to include in calculations.
    typedef typename vec_t::scalar_t scalar_t;  ///< Scalar data type.
  public:
    /**
     * Creates a relax function that has approximately the same values of `dr` as given nodes.
     * @param pos Positions of given nodes.
     * @param values Function values at given positions.
     * @param num_closest How many neighbours to take into account when calculating function value.
     */
    ModifiedSheppardsScatteredInterpolant(const Range<vec_t>& pos, const Range<value_t>& values,
                                          int num_closest) : tree(pos), values(values), num_closest(num_closest) {}

    /// Evaluate the distribution function.
    value_t operator()(const vec_t& point) const {
        Range<int> idx;
        Range<scalar_t> dists2;
        std::tie(idx, dists2) = tree.query(point, num_closest);
        if (dists2[0] < 1e-6) return values[idx[0]];
        value_t avg = value_t(0);
        scalar_t weight_sum = scalar_t(0);
        scalar_t R = std::sqrt(dists2.back());
        for (int i = 0; i < idx.size(); ++i) {
            scalar_t rel_dist = (1 - std::sqrt(dists2[i]) / R);
            scalar_t weight = rel_dist*rel_dist / dists2[i];
            avg += weight*values[idx[i]];
            weight_sum += weight;
        }
        return avg / weight_sum;
    }

    value_t evaluate(const vec_t& point) const {
        Range<int> idx;
        Range<scalar_t> dists2;
        std::tie(idx, dists2) = tree.query(point, num_closest);
        if (dists2[0] < 1e-6) return values[idx[0]];
        value_t avg = value_t(0);
        scalar_t weight_sum = scalar_t(0);
        scalar_t R = std::sqrt(dists2.back());
        for (int i = 0; i < idx.size(); ++i) {
            scalar_t rel_dist = (1 - std::sqrt(dists2[i]) / R);
            scalar_t weight = rel_dist*rel_dist / dists2[i];
            avg += weight*values[idx[i]];
            weight_sum += weight;
        }
        return avg / weight_sum;
    }
};

// SOURCE(t)
VectorXd ricker(double f, double dt) {
    double nw_ = 2.2 / f / dt;
    int nw = 2 * std::floor(nw_ / 2) + 1;
    int nc = std::floor(nw/2);
    VectorXd w = VectorXd(nw);
    VectorXd k = VectorXd(nw);
    VectorXd alpha = VectorXd(nw);
    VectorXd beta = VectorXd(nw);

    for (int i = 0; i < nw; ++i) {
        k[i]= i+1;
        alpha[i] = (nc-k[i]+1)*f*dt*pi;
        beta[i] = alpha[i]*alpha[i];
        w[i] = (1.0 - beta[i]*2)*exp(-beta[i]);
    }
    return w;
}
double distance(Vec2d coord, Vec2d target){
    return std::sqrt(ipow(coord[0] - target[0], 2) + ipow(coord[1] - target[1], 2));
}
double damping(double x, double step,int i_max){
//    prn(-x/(step) -1);
    return ipow( std::exp( -ipow(0.015*(i_max - x/(step) -1), 2) ) ,10);
}

int main(int argc, char* argv[]) {
    // GET PARAMETERS
    if (argc < 2) {
        print_red("Supply parameter file as the second argument.\n");
        return 1;
    }
    XML conf(argv[1]);
    int i_max =20;
    double x_size=conf.get<double>("domain.x");
    double z_size =conf.get<double>("domain.z");
    double step = conf.get<double>("domain.step");
    int n = conf.get<int>("mls.n");  // support size
    int m = conf.get<int>("mls.m");  // monomial basis of second order
    int fill_seed = conf.get<int>("fill.seed");
    double init_heat = conf.get<double>("relax.init_heat");
    double final_heat = conf.get<double>("relax.init_heat");
    double num_neighbours = conf.get<double>("relax.num_neighbours");
    double riter = conf.get<double>("relax.riter");
    double sigma = conf.get<double>("mls.sigma");
    double nodesPerWave = conf.get<double>("domain.nodesPerWave");
    std::string input_path = conf.get<std::string>("IO.in_path");
    std::string hdf_out_filename = conf.get<std::string>("IO.out_path");
    int t_steps = conf.get<int>("problem.time_steps");



    //VELOCITY INTERPOLATION
    HDF hdf_in(input_path);
    auto data = hdf_in.readDouble2DArray("full");
    int num_closest = 10;
    Range<Vec2d> pos;
    Range<double> values;
    for (int i = 0; i < data.size(); i++) {
        Vec2d point({data[i][0],data[i][1]});
        double value = data[i][2];
        pos.push_back(point);
        values.push_back(value);
    }
    ModifiedSheppardsScatteredInterpolant<Vec2d, double> vel_data(pos, values, num_closest);


    // SET dt
    double dt_orig = conf.get<double>("problem.dt_orig");
    double interval = (314) * dt_orig;
    double dt = dt_orig;

    // SETUP SOURCE
    VectorXd wave = ricker(0.001 , 7.0);

    // SOURCE LOCATION
    Vec2d target({0.5*x_size, floor(z_size - 1) - floor(50.0 / 500.0 * z_size)});

    auto fill_density = [&](const Vec2d& p) {
        double L = vel_data.evaluate(p) * interval;
        double suggested_step = L /(3.0 * nodesPerWave);
        return suggested_step;

    };

    // PREPARE DOMAIN
    BoxShape<Vec2d> box({-1.0, -1.0}, {x_size, z_size});

    // Fill with nodes
    std::cout <<"Domain fill\n";
    DomainDiscretization<Vec2d> domain = box.discretizeBoundaryWithDensity(fill_density);
    PoissonDiskSampling<Vec2d> fill; fill.seed(fill_seed).initialPoint(target);
    domain.fill(fill, fill_density);


    // Do relaxation of nodes
    std::cout << "Domain relax\n";
    BasicRelax relax;
    relax.initialHeat(init_heat).finalHeat(final_heat).numNeighbours(num_neighbours).iterations(riter).projectionType(BasicRelax::DO_NOT_PROJECT);
    domain.relax(relax, fill_density);

    // Find support nodes
    std::cout << "Find support\n";
    domain.findSupport(FindClosest(n));

    // SIZE AND LABELS
    int domain_size = domain.size();
    std::cout << "Number of nodes is: " << domain_size << std::endl;

    Range<int> interior = domain.types() > 0;
    Range<int> boundary = domain.types() < 0;

    int target_index=interior[0]; // starting point od PDS!


    // CREATE OUTPUT FILE
    HDF hdf_out(hdf_out_filename, HDF::DESTROY);
    hdf_out.writeDomain("domain", domain);

    // VELOCITY VECTOR
    auto  data_full= hdf_in.readDouble2DArray("full");

    num_closest = 5;
    Range<Vec2d> pos_full;
    Range<double> values_full;
    for (int i = 0; i < data_full.size(); i++) {
        Vec2d point({data_full[i][0],data_full[i][1]});
        double value = data_full[i][2];
        pos_full.push_back(point);
        values_full.push_back(value);
    }
    ModifiedSheppardsScatteredInterpolant<Vec2d, double> vel_data_full(pos_full, values_full, num_closest);

    VectorXd v_list = VectorXd::Zero(interior.size());
    for (int j = 0; j < interior.size(); ++j) {
        int i = interior[j];
        v_list[j]=vel_data_full.evaluate(domain.pos(i));
    }



    // PREPARE OPERATORS AND MATRIX
    // Compute shapes
    std::cout << "Compute shapes\n";
    WLS<Gaussians<Vec2d>, GaussianWeight<Vec2d>, ScaleToClosest>
            approx({m , sigma}, 1);
    auto storage = domain.computeShapes<sh::lap>(approx); //Approximations (shape functions) are computed.



    auto op = storage.explicitOperators();  // operators constructed

    VectorXd T0 = VectorXd::Zero(domain_size); //0-th step
    VectorXd T1 = VectorXd::Zero(domain_size); //1-st step
    VectorXd T2 = VectorXd::Zero(domain_size); //1-st step

    // TIME STEPPING
    int tt;
    int t_save=1;
    Range<Vector2d> out_pos;
    for (int i : interior) {
        out_pos.push_back(domain.pos(i));
    }
    hdf_out.writeDouble2DArray("pos", out_pos);
    hdf_out.openGroup("/step" + std::to_string(t_save));
    hdf_out.writeDoubleAttribute("TimeStep", t_save);
    hdf_out.writeDoubleAttribute("time", 0.0);

    VectorXd TI1 = VectorXd::Zero(domain.interior().size()); //1-st step: interior
    int j=0;
    for (int i : interior) {
        TI1[j] = T1[i];
        j++;
    }
    hdf_out.writeDoubleArray("E", TI1);
    ++t_save;

    // Time stepping
    std::cout << "Begin time stepping\n";
    for (tt = 1; tt <= t_steps; ++tt) {
        std::cout << "Time step " << tt << " of " << t_steps << std::endl;
        //solve
        for (int j = 0; j < interior.size(); ++j) {
            int i = interior[j];
            T2[i] = 2* T1[i] - T0[i] + dt * dt * ipow(v_list[j], 2)  * op.lap(T1, i);
        }
        // SOURCE
        if (tt < wave.size()) {
            T2[target_index] +=  2.5*wave[tt];
        }

        // TIME ADVANCE
        T0=T1;
        T1=T2;

        //ABSORBING BOUNDARY
        for (int j = 0; j < interior.size(); ++j) {
            int i = interior[j];
            double x = domain.pos(i, 0);
            double z = domain.pos(i, 1);
            if (x <= (i_max-1)*step){
                T0[i]*=damping(x,step,i_max);
                T1[i]*=damping(x,step,i_max);
            } else {
                if  (x >= ((x_size-1)-(i_max-1)*step) ){
                    T0[i]*=damping((x_size-1)-x, step, i_max);
                    T1[i]*=damping((x_size-1)-x, step, i_max);
                } else {
                    if (z < (i_max-1)*step){
                        T0[i]*=damping(z,step,i_max);;
                        T1[i]*=damping(z,step,i_max);;
                    }
                }
            }
        }

        if ((tt+1)%10==0){ //change condition when not saving every step
            hdf_out.openGroup("/step" + std::to_string(t_save));
            hdf_out.writeDoubleAttribute("TimeStep", t_save);
            j=0;
            for (int i : interior) {
                TI1[j]= T1[i];
                j++;
            }
            hdf_out.writeDoubleArray("E", TI1);
            hdf_out.writeDoubleAttribute("time", dt * tt);
        }
        ++t_save;

    }

    // DISPLAY COMPUTATION TIME
    hdf_out.openGroup("/");

    hdf_out.openGroup("/");
    hdf_out.openGroup("/params");
    hdf_out.writeDoubleAttribute("N", domain_size);
    hdf_out.writeDoubleAttribute("dt", dt);
    hdf_out.writeDoubleAttribute("dt_orig", dt_orig);
    hdf_out.writeDoubleAttribute("step", step);
    hdf_out.writeDoubleAttribute("t_steps", t_steps);
    hdf_out.writeDoubleAttribute("n", n);
    hdf_out.writeDoubleAttribute("m", m);
    hdf_out.writeDoubleAttribute("sigma", sigma);
}
