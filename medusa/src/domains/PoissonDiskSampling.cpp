#include <medusa/bits/domains/PoissonDiskSampling.hpp>
#include <medusa/bits/domains/DomainDiscretization_fwd.hpp>
#include <medusa/bits/types/Vec.hpp>

/**
 * @file
 * Instantiations of Poisson disk sampling algorithm.
 */

template class mm::PoissonDiskSampling<mm::Vec1d>;
template class mm::PoissonDiskSampling<mm::Vec2d>;
template class mm::PoissonDiskSampling<mm::Vec3d>;
