#ifndef MEDUSA_BITS_UTILS_PRINT_HPP_
#define MEDUSA_BITS_UTILS_PRINT_HPP_

/**
 * @file
 * Printing helpers for std types.
 */

#include <iostream>
#include <vector>
#include <array>
#include <utility>

// additional ostream operators
namespace std {

/// Output pairs as `(1, 2)`.
template<class T, class U>
std::ostream& operator<<(std::ostream& xx, const std::pair<T, U>& par) {
    return xx << "(" << par.first << "," << par.second << ")";
}

/// Output arrays as `[1, 2, 3]`.
template<class T, size_t N>
std::ostream& operator<<(std::ostream& xx, const std::array<T, N>& arr) {
    xx << "[";
    for (size_t i = 0; i < N; ++i) {
        xx << arr[i];
        if (i < N - 1) xx << ", ";
    }
    xx << "]";
    return xx;
}

/// Output vectors as `[1, 2, 3]`.
template<class T, class A>
std::ostream& operator<<(std::ostream& xx, const std::vector<T, A>& arr) {
    // do it like the matlab does it.
    xx << "[";
    for (size_t i = 0; i < arr.size(); ++i) {
        xx << arr[i];
        if (i < arr.size() - 1) xx << ", ";
    }
    xx << "]";
    return xx;
}

/// Output nested vectors as `[[1, 2]; [3, 4]]`.
template<class T, class A>
std::ostream& operator<<(std::ostream& xx, const std::vector<std::vector<T, A>>& arr) {
    xx << "[";
    for (size_t i = 0; i < arr.size(); ++i) {
        for (size_t j = 0; j < arr[i].size(); ++j) {
            xx << arr[i][j];
            if (j < arr[i].size() - 1) xx << ", ";
        }
        if (i < arr.size() - 1) xx << "; ";
    }
    xx << "]";
    return xx;
}

}  // namespace std

#endif  // MEDUSA_BITS_UTILS_PRINT_HPP_
