#ifndef MEDUSA_BITS_DOMAINS_CIRCLESHAPE_HPP_
#define MEDUSA_BITS_DOMAINS_CIRCLESHAPE_HPP_

#include "CircleShape_fwd.hpp"
#include <medusa/bits/utils/assert.hpp>
#include "DomainDiscretization.hpp"
#include "PoissonDiskSampling.hpp"
#include <medusa/bits/utils/numutils.hpp>

/**
 * @file
 * Implementation of class for ball shaped domains.
 */

namespace mm {

template <typename vec_t>
DomainDiscretization<vec_t> CircleShape<vec_t>::discretizeBoundaryWithStep(scalar_t step,
                                                                           int type) const {
    static_assert(dim <= 3, "Discretization of surfaces of spheres of dimension >=4 is "
                            "currently not supported.");
    if (type == 0) type = -1;
    DomainDiscretization<vec_t> domain(*this);
    vec_t normal;
    if (dim == 1) {  // always only two points
        domain.addBoundaryNode({center_[0] - radius_}, type, vec_t(-1.0));
        domain.addBoundaryNode({center_[0] + radius_}, type, vec_t(1.0));
    } else if (dim == 2) {
        // normal uniform distribution by angle
        int num_of_points = iceil(PI / std::asin(step/2.0/radius_));
        scalar_t dfi = 2*PI / num_of_points;
        for (int i = 0; i < num_of_points; ++i) {
            normal[0] = std::cos(i * dfi); normal[1] = std::sin(i * dfi);
            domain.addBoundaryNode(center_ + radius_ * normal, type, normal);
        }
    } else if (dim == 3) {  // Fibonacci spheres
        // code from http://stackoverflow.com/questions/9600801
        // explanation: http://blog.marmakoide.org/?p=1
        int num_of_points = iceil(4*PI*radius_*radius_ / step / step);
        scalar_t offset = 2.0 / num_of_points;
        scalar_t increment = M_PI * (3.0 - std::sqrt(5));
        scalar_t x, y, z, r, phi;
        for (int i = 0; i < num_of_points; ++i) {
            y = ((i * offset) - 1) + (offset / 2);
            r = std::sqrt(1 - y * y);
            phi = i * increment;
            x = std::cos(phi) * r;
            z = std::sin(phi) * r;
            normal[0] = x; normal[1] = y; normal[2] = z;
            domain.addBoundaryNode(center_ + radius_ * normal, type, normal);
        }
    }
    return domain;
}

template <typename vec_t>
DomainDiscretization<vec_t> CircleShape<vec_t>::discretizeWithStep(
        scalar_t step, int internal_type, int boundary_type, FillEngine<vec_t>* fill) const {
    auto d = discretizeBoundaryWithStep(step, boundary_type);
    static_assert(0 < dim && dim <= 3, "Only dimensions up to 3 are supported.");
    if (internal_type == 0) internal_type = 1;
    if (dim == 1) {
        int num_of_points = iceil(2*radius_/step) - 1;
        for (const vec_t& v : linspace(bbox().first, bbox().second, {num_of_points}, false)) {
            d.addInternalNode(v, internal_type);
        }
    } else if (dim == 2) {  // Fill circle using Fibonacci spiral.
        // Use concentric circles with step dx.
        int num_of_circles = iceil(radius_ / step);  // not counting outer circle
        scalar_t dr = radius_ / num_of_circles;
        d.addInternalNode(center_, internal_type);
        for (int i = 1; i < num_of_circles; ++i) {
            scalar_t r = i*dr;
            int num_of_points = iceil(PI / std::asin(step/2.0/r));
            scalar_t dfi = 2*PI / num_of_points;
            for (int j = 0; j < num_of_points; ++j) {
                d.addInternalNode(center_ + r * vec_t({std::cos(j*dfi), std::sin(j*dfi)}),
                                  internal_type);
            }
        }
    } else if (dim == 3) {  // Uniform cuboid, taking ones inside
        return DomainShape<vec_t>::discretizeWithStep(step, internal_type, boundary_type, fill);
    }
    return d;
}

template <typename vec_t>
std::ostream& CircleShape<vec_t>::print(std::ostream& os) const {
    return os << "CircleShape(" << center_.transpose() << ", " << radius_ << ")";
}

template <typename vec_t>
DomainDiscretization<vec_t>
CircleShape<vec_t>::discretizeBoundaryWithDensity(const std::function<scalar_t(vec_t)>& dr,
                                                  int type) const {
    static_assert(0 < dim && dim <= 3, "Only dimensions up to 3 are supported.");
    if (type == 0) type = -1;
    DomainDiscretization<vec_t> domain(*this);
    if (dim == 1) {  // always only two points
        domain.addBoundaryNode({center_[0] - radius_}, type, vec_t(-1.0));
        domain.addBoundaryNode({center_[0] + radius_}, type, vec_t(1.0));
    } else if (dim == 2) {
        scalar_t cur_alpha = 0.0;
        scalar_t max_alpha = 2*PI;
        Range<scalar_t> alphas = {cur_alpha};
        while (cur_alpha < max_alpha) {
            scalar_t r = dr(center_ + radius_ * vec_t({std::cos(cur_alpha), std::sin(cur_alpha)}));
            cur_alpha += 2*std::asin(r/2.0/radius_);
            alphas.push_back(cur_alpha);
        }

        scalar_t shrink = 2*PI / cur_alpha;
        for (int i = 0; i < alphas.size()-1; ++i) {
            scalar_t a = alphas[i] * shrink;
            vec_t normal = {std::cos(a), std::sin(a)};
            domain.addBoundaryNode(center_ + radius_ * normal, type, normal);
        }
    } else if (dim == 3) {
        // define modified density
        auto dr2upper = [&](const Vec<scalar_t, 2>& v) {
            double d = (1 + v.squaredNorm());
            vec_t p = {2*v[0]/d, 2*v[1]/d, (1-v.squaredNorm())/d};
            return d/2 * dr(center_ + radius_ * p) / radius_;
        };

        CircleShape<Vec<scalar_t, 2>> circ(0.0, 1.0);
        PoissonDiskSampling<Vec<scalar_t, 2>> fill; fill.seed(0).proximityFactor(0.99);
        auto domain2d = circ.discretizeWithDensity(dr2upper, &fill);

        for (int i : domain2d.interior()) {
            auto v = domain2d.pos(i);
            double d = (1 + v.squaredNorm());
            vec_t p = {2*v[0]/d, 2*v[1]/d, (1-v.squaredNorm())/d};
            domain.addBoundaryNode(center_ + radius_*p, type, p.normalized());
        }

        for (int i : domain2d.boundary()) {
            auto v = domain2d.pos(i);
            vec_t p = {v[0], v[1], 0};
            domain.addBoundaryNode(center_ + radius_*p, type, p.normalized());
        }

        // lower hemisphere
        auto dr2lower = [&](const Vec<scalar_t, 2>& v) {
            double d = (1 + v.squaredNorm());
            vec_t p = {2*v[0]/d, 2*v[1]/d, -(1-v.squaredNorm())/d};
            return d/2 * dr(center_ + radius_ * p) / radius_;
        };
        domain2d = circ.discretizeWithDensity(dr2lower, &fill);
        for (int i : domain2d.interior()) {
            auto v = domain2d.pos(i);
            double d = (1 + v.squaredNorm());
            vec_t p = {2*v[0]/d, 2*v[1]/d, -(1-v.squaredNorm())/d};
            domain.addBoundaryNode(center_ + radius_*p, type, p.normalized());
        }
    }
    return domain;
}

}  // namespace mm

#endif  // MEDUSA_BITS_DOMAINS_CIRCLESHAPE_HPP_
